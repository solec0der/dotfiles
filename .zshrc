# Path to your oh-my-zsh installation.
export ZSH="/home/pi/.oh-my-zsh"
export LC_ALL=en_US.UTF-8

ZSH_THEME="robbyrussell"

alias v="nvim"


alias gstat="git status"
alias gaa="git add ."

export LD_LIBRARY_PATH=/usr/local/lib

export VISUAL=nvim
export EDITOR="$VISUAL"

plugins=(git)

source $ZSH/oh-my-zsh.sh


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
