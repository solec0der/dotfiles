"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-fugitive'
Plug 'ludovicchabant/vim-gutentags'

" Autocompletion
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'

Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'

" PHP Specific Autocompletion
Plug 'phpactor/ncm2-phpactor'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'Valloric/MatchTagAlways'
Plug 'tpope/vim-commentary'

Plug 'StanAngeloff/php.vim'
Plug 'phpactor/phpactor', {'for': 'php', 'do': 'composer install --no-dev -o'}
Plug 'stephpy/vim-php-cs-fixer'
Plug 'leafgarland/typescript-vim'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'https://github.com/vim-syntastic/syntastic'
Plug 'editorconfig/editorconfig-vim'

Plug 'HerringtonDarkholme/yats.vim'

Plug 'vimwiki/vimwiki'

Plug 'prettier/vim-prettier', { 'do': 'npm ci' }

Plug 'morhetz/gruvbox'

Plug 'christoomey/vim-system-copy'
Plug 'jiangmiao/auto-pairs'

call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General VIM Configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set background=dark

set nocompatible
filetype plugin on

set relativenumber

set smarttab
set tabstop=4

set cindent
set shiftwidth=2
set expandtab
set encoding=utf-8
set nowrap
set hlsearch
set incsearch
set ignorecase
set smartcase
set mouse=n
set path+=**
set wildmenu
set colorcolumn=120
colorscheme gruvbox

let g:deoplete#enable_at_startup = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Airline configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Make airline to appear on all the tabs
set laststatus=2

let g:airline_theme='tomorrow'

let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#format = 1
let g:airline#extensions#branch#displayed_head_limit = 13



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Remappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = ","
:command WQ wq
:command Wq wq
:command W w
:command Q q
:command Qa qa

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => fzf
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <C-p> :GFiles<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Nerdtree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:NERDTreeGitStatusWithFlags = 1
"let g:WebDevIconsUnicodeDecorateFolderNodes = 1
"let g:NERDTreeGitStatusNodeColorization = 1
"let g:NERDTreeColorMapCustom = {
    "\ "Staged"    : "#0ee375",  
    "\ "Modified"  : "#d9bf91",  
    "\ "Renamed"   : "#51C9FC",  
    "\ "Untracked" : "#FCE77C",  
    "\ "Unmerged"  : "#FC51E6",  
    "\ "Dirty"     : "#FFBD61",  
    "\ "Clean"     : "#87939A",   
    "\ "Ignored"   : "#808080"   
    "\ }                         


map <leader>l :NERDTreeToggle<CR>

let g:NERDTreeIgnore = ['^node_modules$']

nmap <C-l> :call phpactor#UseAdd()<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim Viki
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:vimwiki_list = [{'path': '~/Documents/notes/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
augroup Markdown
  autocmd!
  autocmd FileType markdown set wrap linebreak nolist
augroup END

vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y

autocmd BufEnter * call ncm2#enable_for_buffer()

set completeopt=noinsert,menuone,noselect
"
